#!/usr/bin/env ksh

set -e

# deps for an imd service
pkg_add xmlstarlet ipcalc bash

# eh, add the guest agent too
pkg_add qemu-ga

# this is because openbsd dhclient is...kinda dumb
printf 'request interface-mtu;\n' > /etc/dhclient.conf

# this is...actually getting the install subroutine back from our bsd.rd
WRKDIR=$(mktemp -d)
rdsetroot -x /bsd.rd "${WRKDIR}/ramdisk" || {
  zcat /bsd.rd > "${WRKDIR}/bsd.rd"
  rdsetroot -x "${WRKDIR}/bsd.rd" "${WRKDIR}/ramdisk"
}
mkdir "${WRKDIR}/ramdisk.d"
vnconfig vnd0 "${WRKDIR}/ramdisk"
mount -o nodev,nosuid,noexec /dev/vnd0a "${WRKDIR}/ramdisk.d"
cp "${WRKDIR}/ramdisk.d/install.sub" /usr/local/libexec
umount "${WRKDIR}/ramdisk.d"
vnconfig -u vnd0
rm -rf "${WRKDIR}"

# always fix perms on imx
chmod 0750 /usr/local/sbin/imx.sh
chown 0:0  /usr/local/sbin/imx.sh
