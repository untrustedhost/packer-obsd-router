#!/usr/bin/env sh

echo 'check/wait for reorder_kernel'
sleep 2
while pgrep -qxf '/bin/ksh .*reorder_kernel' ; do echo . ; sleep 1 ; done

# list for information
syspatch -l | true

# do
syspatch | true
