#!/usr/bin/env ksh

set -e

pkg_add augeas

fstab_run="$(augtool print '/files/etc/fstab/*[file="/run"]/file' | awk -F' = ' '{print substr($2,2,length($2) -2)}')"

[ -z "${fstab_run}" ] && {
 echo "swap /run mfs rw,nodev,nosuid,-s=4m 0 0" >> /etc/fstab
}

mkdir -p /run

#augtool set '/files/etc/fstab/*[file="/run"]/spec' 'swap'
#augtool set '/files/etc/fstab/*[file="/run"]/vfstype' 'mfs'
#augtool set '/files/etc/fstab/*[file="/run"]/dump' '0'
#augtool set '/files/etc/fstab/*[file="/run"]/passno' '0'
