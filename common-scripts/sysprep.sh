#!/usr/bin/env sh

set -e

# configure machine to reboot on panic
printf 'ddb.panic=0\n' >> /etc/sysctl.conf

# also stabilize perms
chmod 0600 /etc/sysctl.conf

# set any imx pieces +x - and it's ok if we don't have any
chmod 0700 /usr/local/libexec/imd_*.sh || true

# until we replace 18.04 drivers, configure sshd
augtool set /files/etc/ssh/sshd_config/PubkeyAcceptedKeyTypes +ssh-rsa

# clean out state for cloning
rm /etc/hostname.*
rm /etc/ssh/ssh_host_*key
rm /etc/random.seed
