#!/usr/bin/env bash

set -eu

OBSD_VERSION=${OBSD_VERSION:-6.5}
OBSD_V_NDOTS=${OBSD_VERSION//./}
OBSD_ARCH=amd64
OBSD_MIRROR="${OBSD_MIRROR:-https://cdn.openbsd.org/pub/OpenBSD}"
OBSD_URL="${OBSD_URL:-${OBSD_MIRROR}/${OBSD_VERSION}/${OBSD_ARCH}}"
SYSPATCH_URL="${SYSPATCH_URL:-${OBSD_MIRROR}/syspatch/${OBSD_VERSION}/${OBSD_ARCH}}"
LOCAL_DIST="obsd_dist/${OBSD_VERSION}/${OBSD_ARCH}"
SYSPATCH_DIST="obsd_dist/syspatch/${OBSD_VERSION}/${OBSD_ARCH}"
LOCAL_SIGS="obsd_signatures"
MAX_TRIES=5

type signify-openbsd 2>/dev/null 1>&2 && signify() { command signify-openbsd "${@}" ; }
type signify 2>/dev/null 1>&2 || { echo "could not find signify" 1>&2 ; exit 1 ; }

[ -n "${1:-}" ] || { echo "supply file to download" 1>&2 ; exit 1 ; }


check_meta () {
  local ct dir sig okay selfsig sfile url
  dir="${1}" ; sfile="${2}" ; url="${3}" ; ct="${MAX_TRIES}" ; selfsig=''
  okay='Signature Verified'
  while [ "${selfsig}" != "${okay}" ] ; do
    [ "${ct}" == 0 ] && return 1
    selfsig="$(signify -C -p "${sfile}" -x "${dir}/SHA256.sig" 2>/dev/null | head -n1)"
    [ "${selfsig}" == "${okay}" ] && continue
    ct=$((ct - 1))
    curl -L -o "${dir}/SHA256.sig" "${url}/SHA256.sig"
  done
}

check_signify () {
  local dir file sig
  dir="${1}" ; file="${2}" ; sig="${3}"
  (cd "${dir}" && signify -C -p "${sig}" -x "SHA256.sig" "${file}")
}

file="${1}"
case "${file}" in
  syspatch) 
    sigfile="$(pwd)/${LOCAL_SIGS}/openbsd-${OBSD_V_NDOTS}-syspatch.pub"
    [ -e "${sigfile}" ] || { echo "could not find pubkey" 1>&2 ; exit 1 ; }
    mkdir -p "${SYSPATCH_DIST}"
    rm -f "${SYSPATCH_DIST}/SHA256.sig"
    check_meta "${SYSPATCH_DIST}" "${sigfile}" "${SYSPATCH_URL}"
    while read -r patchline ; do
      case "${patchline}" in
        SHA256\ *) patchfile="${patchline%%)*}" ; patchfile="${patchfile##*(}"
          ct=${MAX_TRIES}
          until check_signify "${SYSPATCH_DIST}" "${patchfile}" "${sigfile}" ; do
            [ "${ct}" == 0 ] && exit 1
            ct=$((ct - 1))
            curl -L -o "${SYSPATCH_DIST}/${patchfile}" "${SYSPATCH_URL}/${patchfile}"
          done
        ;;
      esac
    done < "${SYSPATCH_DIST}/SHA256.sig"
  ;;
  srcpatch)
    # this is an unsigned file, the patches *are* sigfiles...
    sigfile="$(pwd)/${LOCAL_SIGS}/openbsd-${OBSD_V_NDOTS}-base.pub"
    mkdir -p "obsd_dist/patches/"
    # check if file would exist...
    ball="${OBSD_MIRROR}/patches/${OBSD_VERSION}.tar.gz"
    curl --output /dev/null --silent --head --fail "${ball}" && {
      # get
      scratch=$(mktemp)
      extract=$(mktemp -d)
      curl -L -o "${scratch}" "${OBSD_MIRROR}/patches/${OBSD_VERSION}.tar.gz"
      # test
      tar xzf "${scratch}" -C "${extract}"
      # validate...
      find "${extract}" -type f -print0 | xargs -0 -n 1 signify -V -p "${sigfile}" -e -m /dev/null -x
      # finally, replace the patch ball
      mv "${scratch}" "obsd_dist/patches/${OBSD_VERSION}.tar.gz"
      # cleanup
      rm -rf "${scratch}" "${extract}"
    }
  ;;
  *.tar.gz)
    sigfile="$(pwd)/${LOCAL_SIGS}/openbsd-${OBSD_V_NDOTS}-base.pub"
    [ -e "${sigfile}" ] || { echo "could not find pubkey" 1>&2 ; exit 1 ; }
    _dist="obsd_dist/${OBSD_VERSION}"
    mkdir -p "$_dist"
    check_meta "$_dist" "${sigfile}" "${OBSD_MIRROR}/${OBSD_VERSION}"
    ct=${MAX_TRIES}
    until check_signify "${_dist}" "${file}" "${sigfile}" ; do
      [ "${ct}" == 0 ] && exit 1
      ct=$((ct - 1))
      curl -L -o "${_dist}/${file}" "${OBSD_MIRROR}/${OBSD_VERSION}/${file}"
    done
  ;;
  *)
    sigfile="$(pwd)/${LOCAL_SIGS}/openbsd-${OBSD_V_NDOTS}-base.pub"
    [ -e "${sigfile}" ] || { echo "could not find pubkey" 1>&2 ; exit 1 ; }
    mkdir -p "${LOCAL_DIST}"
    _dist="${LOCAL_DIST}"
    check_meta "${LOCAL_DIST}" "${sigfile}" "${OBSD_URL}"
    ct=${MAX_TRIES}
    until check_signify "${_dist}" "${file}" "${sigfile}" ; do
      [ "${ct}" == 0 ] && exit 1
      ct=$((ct - 1))
      curl -L -o "${LOCAL_DIST}/${file}" "${OBSD_URL}/${file}"
    done
  ;;
esac
