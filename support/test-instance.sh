#!/usr/bin/env bash

# read a libvirt xml and glue to qemu-user using multicast tomfoolery.

# fixed ports
declare -A fmp
fmp[cyan]='2000'
fmp[magenta]='2001'
fmp[yellow]='2002'

# map to user
user_netdevs=('vmm')

# start of dynamic port range
#dynamic_mp_range=4000

metadata_xml="${1}"
drive="${2}"
metadata_iso="${3}"

qemu_args=()

# initial args to qemu...
qemu_args=('-nographic' '-drive' "file=${drive},if=virtio" '--enable-kvm' '-cdrom' "${metadata_iso}")

# get memory and convert to MiB
ram=$(xmlstarlet sel -t -v 'domain/memory' < "${metadata_xml}")
ram=$((ram / 1024))

qemu_args=("${qemu_args[@]}" "-m" "${ram}")

# get values from metadata xml now...
for bridge in $(xmlstarlet sel -t -v 'domain/devices/interface/source/@bridge' < "${metadata_xml}") ; do
  mac=''
  mac="$(xmlstarlet sel -t -v "domain/devices/interface[source/@bridge=\"${bridge}\"]/mac/@address" < "${metadata_xml}")"
  # we're just going to ignore things that have no mac address for now.
  [[ "${mac}" ]] && {
    # is it a user-mapping device?
    case " ${user_netdevs[*]} " in
      " ${bridge} ") qemu_args=("${qemu_args[@]}" '-device' "virtio-net-pci,netdev=user,mac=${mac}" '-netdev' 'user,id=user') ; continue ;;
    esac
    # is it on a fixed port?
    [[ "${fmp[${bridge}]}" ]] && {
      qemu_args=("${qemu_args[@]}" '-device' "virtio-net-pci,netdev=${bridge},mac=${mac}" '-netdev' "socket,id=${bridge},mcast=230.0.0.1:${fmp[${bridge}]}")
    }
  }
  echo "${mac}"
done

qemu-system-x86_64 "${qemu_args[@]}"
