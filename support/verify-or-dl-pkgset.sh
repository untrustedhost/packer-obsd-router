#!/usr/bin/env bash

set -eu

OBSD_VERSION=${OBSD_VERSION:-6.5}
OBSD_V_NDOTS=${OBSD_VERSION//./}
OBSD_ARCH=amd64
OBSD_MIRROR="${OBSD_MIRROR:-https://cdn.openbsd.org/pub/OpenBSD}"
PACKAGE_URL="${OBSD_URL:-${OBSD_MIRROR}/${OBSD_VERSION}/packages/${OBSD_ARCH}}"
PACKAGE_DIST="obsd_dist/${OBSD_VERSION}/packages/${OBSD_ARCH}"
LOCAL_SIGS="obsd_signatures"
MAX_TRIES=5

type signify-openbsd 2>/dev/null 1>&2 && signify() { command signify-openbsd "${@}" ; }
type signify 2>/dev/null 1>&2 || { echo "could not find signify" 1>&2 ; exit 1 ; }

[ -n "${1:-}" ] || { echo "supply package to download" 1>&2 ; exit 1 ; }


check_meta () {
  local ct dir sig okay selfsig sfile url
  dir="${1}" ; sfile="${2}" ; url="${3}" ; ct="${MAX_TRIES}" ; selfsig=''
  okay='Signature Verified'
  while [ "${selfsig}" != "${okay}" ] ; do
    [ "${ct}" == 0 ] && return 1
    selfsig="$(signify -C -p "${sfile}" -x "${dir}/SHA256.sig" 2>/dev/null | head -n1)"
    [ "${selfsig}" == "${okay}" ] && continue
    ct=$((ct - 1))
    curl -L -o "${dir}/SHA256.sig" "${url}/SHA256.sig"
  done
}

check_signify () {
  local dir file sig
  dir="${1}" ; file="${2}" ; sig="${3}"
  (cd "${dir}" && signify -C -p "${sig}" -x "SHA256.sig" "${file}")
}

packagename="${1}"

sigfile="$(pwd)/${LOCAL_SIGS}/openbsd-${OBSD_V_NDOTS}-pkg.pub"
[ -e "${sigfile}" ] || { echo "could not find pubkey" 1>&2 ; exit 1 ; }
mkdir -p "${PACKAGE_DIST}"
check_meta "${PACKAGE_DIST}" "${sigfile}" "${PACKAGE_URL}"
rp=$(grep -F " (${packagename}" "${PACKAGE_DIST}/SHA256.sig" | cut -d' ' -f2 | head -n1)
rp="${rp#(}"
rp="${rp%)}"
ct=${MAX_TRIES}
until check_signify "${PACKAGE_DIST}" "${rp}" "${sigfile}" ; do
  [ "${ct}" == 0 ] && exit 1
  ct=$((ct - 1))
  curl -L -o "${PACKAGE_DIST}/${rp}" "${PACKAGE_URL}/${rp}"
done

deplist=$(tar xOf "${PACKAGE_DIST}/${rp}" +CONTENTS|grep ^@depend|cut -d: -f3)

for pkg in ${deplist} ; do
  bash "${BASH_SOURCE[0]}" "${pkg}"
done
