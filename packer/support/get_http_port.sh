#!/usr/bin/env bash

portmin=8000
portmax=9000
range=$((portmax - portmin))
maxtries=10

# pick random port in range, check for usage, return it
while [ "${maxtries}" != 0 ] ; do
  try=$(((RANDOM % $range) + $portmin ))
  nc -z 127.0.0.1 "${try}" || { echo "${try}" ; exit 0 ; }
  maxtries=$((maxtries - 1))
done

exit 1
