#!/usr/bin/env ksh

# configure the rest of miniupnpd to function...

# assuming the interface came up, grab the egress ifname
egress_ifn=$(ifconfig egress|grep flags|cut -d: -f1)
[[ -n "${egress_ifn}" ]] || exit 0

# get the ingress ifname from the same type of labeling
ingress_ifn=$(ifconfig upnp|grep flags|grep -v ^carp|cut -d: -f1)
[[ -n "${ingress_ifn}" ]] || exit 0

{
  printf 'ext_ifname=%s\n' "${egress_ifn}"
  printf 'listening_ip=%s\n' "${ingress_ifn}"
} >> /etc/miniupnpd.conf

rcctl enable miniupnpd
rcctl start miniupnpd

# don't allow failure to stop imx here
exit 0
