#!/usr/bin/env ksh

set -x

# flip forwarding on past this point
printf 'net.inet.ip.forwarding=1\n' >> /etc/sysctl.conf

# install miniupnpd
pkg_add miniupnpd

# replace pf.conf with ours
mv ./pf.conf /etc/pf.conf
mkdir -p /etc/pf
touch /etc/pf/dhclient.pf
mv ./int_if.macro /etc/pf/int_if.macro
mv ./blockpol.macro /etc/pf/blockpol.macro
mv ./logiface.macro /etc/pf/logiface.macro
mv ./maxmss.macro /etc/pf/maxmss.macro
mv ./staticnat.macro /etc/pf/staticnat.macro
mv ./staticnat_neg.macro /etc/pf/staticnat_neg.macro
mv ./egress-nat.dist /etc/pf/egress-nat.dist
touch /etc/pf/dhcpservers.table
touch /etc/pf/nat.pf
touch /etc/pf/ospf.pf
touch /etc/pf/carp.pf
touch /etc/pf/additional.pf

# replace miniupnp conf with ours
mv ./miniupnpd.conf /etc/miniupnpd.conf

cp ./imd_*.sh /usr/local/libexec
chmod +x /usr/local/libexec/imd_*.sh
rm -f ./imd_*.sh
