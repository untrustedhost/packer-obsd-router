#!/usr/bin/env ksh

# lifted from install.subr
isin() {
  local _a=$1 _b

  shift
  for _b; do
    [[ $_a == "$_b" ]] && return 0
  done
  return 1
}

rmel() {
  local _a=$1 _b _c

  shift
  for _b; do
    [[ $_a != "$_b" ]] && _c="${_c:+$_c }$_b"
  done
  echo -n "$_c"
}

get_ifs() {
  local _if _if_list=$(rmel vlan $(ifconfig -C))

  for _if in $(ifconfig "$@" 2>/dev/null | sed '/^[a-z]/!d;s/:.*//'); do
    isin "${_if%%+([0-9])}" $_if_list || echo $_if
  done
}

# get a list of all interfaces and MAC addresses
if_mlist=''
for intf in $(get_ifs) ; do
  if_lla=''
  if_lla=$(ifconfig "${intf}" | awk '$1 == "lladdr" { print $2 ; }')
  if_mlist="${if_mlist} ${intf}:${if_lla}"
done

# find all the tagged carp interfaces to wire in to ospf.conf
if_pre='metadata/domain/devices/interface'

# actually, if you don't have a router/@id, go away
router_id=''
router_id="$(xml sel -t -v metadata/router/@id < "${IMD_MDDIR}/MDDATA.XML")"

# which should always be an IPv4 octet-notation address..
router_ip_id=''
router_ip_id="$(ipcalc "${router_id}/32" | awk -F': ' '$1 ~ "address" { print $2 }' | xargs | cut -d' ' -f1)"

[[ "${router_ip_id}" == '' ]] && exit 0 ;

# set mask
umask 077

# set id, and _never_ redistribute APIPA
{ printf 'router-id %s\n' "${router_ip_id}"
  printf 'no redistribute 169.254.169/24\n'
  printf 'no redistribute 169.254/16\n'
} > /etc/ospfd.conf

# grab interfaces by vhid ( which has to be unique... )
for vhid in $(xml sel -t -v "${if_pre}[carp4/@redistribute_ospf=\"true\"]/carp4/@vhid" < "${IMD_MDDIR}/MDDATA.XML") ; do
  carp_addr='' ; net_prefix=''
  carp_addr=$(xml sel -t -v "${if_pre}[carp4/@vhid=\"${vhid}\"]/carp4/@address" < "${IMD_MDDIR}/MDDATA.XML")
  net_prefix=$(ipcalc "${carp_addr}" | awk -F': ' '$1 ~ "network" { print $2 }' | xargs | cut -d' ' -f1)
  net_mask=$(echo "${carp_addr}" | cut -f2 -d/)
  [[ "${net_prefix}" != '' ]] && { printf 'redistribute %s/%s depend on carp%s\n' "${net_prefix}" "${net_mask}" "${vhid}" >> /etc/ospfd.conf ; }
done

# grab all the ospf areas, sorted and uniq'd
ospf_areas=''
ospf_areas="$(xml sel -t -v "${if_pre}/ospf/@area" < "${IMD_MDDIR}/MDDATA.XML" | sort | uniq)"

# walk those...
for area in ${ospf_areas} ; do
  # for each area, get the mac address of the interfaces for that area
  macs_in_ospf=''
  macs_in_ospf="$(xml sel -t -v "${if_pre}[ospf/@area=\"${area}\"]/mac/@address" < "${IMD_MDDIR}/MDDATA.XML")"
  ifs_in_ospf=''

  # resolve interfaces to names via mac addresses
  for mac in ${macs_in_ospf} ; do
    for ent in ${if_mlist} ; do
      ifn=''
      case "${ent}" in
        *":${mac}") ifn=$(echo "${ent}" |cut -d: -f1) ; ifs_in_ospf="${ifs_in_ospf} ${ifn}" ;;
      esac
    done
  done

  # write to config
  {
    printf 'area %s {\n' "${area}"
      for ifn in ${ifs_in_ospf} ; do
        printf '  interface %s {\n' "${ifn}"
          keys_in_ospf=''
          keys_in_ospf="$(xml sel -t -v "metadata/router/ospf[@area=\"${area}\"]/authentication/@key" < "${IMD_MDDIR}/MDDATA.XML")"
          [[ "${keys_in_ospf}" != '' ]] && {
            printf '    auth-type crypt\n'
            for key in ${keys_in_ospf} ; do
              ospf_pass=''
              ospf_pass="$(xml sel -t -v "metadata/router/ospf[@area=\"${area}\"]/authentication[@key=\"${key}\"]/@password" < "${IMD_MDDIR}/MDDATA.XML")"
              [[ "${ospf_pass}" != '' ]] && {
                printf '    auth-md %s "%s"\n'  "${key}" "${ospf_pass}"
                printf '    auth-md-keyid %s\n' "${key}"
              }
            done
          }
        printf '  }\n'
      done
    printf '}\n'
  } >> /etc/ospfd.conf
done

# validate config
ospfd -dn || exit 1

# enable, start ospfd
rcctl enable ospfd
rcctl start ospfd
