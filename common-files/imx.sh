#!/usr/bin/env ksh

# first, try to see if cd0c is an IMD volume
imvol=$(disklabel /dev/cd0c |grep ^disk:\ |cut -d' ' -f2)
[[ "${imvol}" == 'IMD' ]] || exit 1

# get a scratch directory...
scratch=$(mktemp -d)

# mount the cd
mount -o ro /dev/cd0c "${scratch}"

# check for file
[[ -f "${scratch}/MDDATA.XML" ]] || { umount "${scratch}" ; exit 1 ; }

export IMD_MDDIR="${scratch}"

PATH="${PATH}:/usr/local/bin"

export PATH

for f in /usr/local/libexec/imd_* ; do
  [[ -x "${f}" ]] && { "${f}" || { umount "${scratch}" ; exit 1 ; } ; }
done

umount "${scratch}"
