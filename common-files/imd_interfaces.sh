#!/usr/bin/env ksh

# lifted from install.subr
isin() {
  local _a=$1 _b

  shift
  for _b; do
    [[ $_a == "$_b" ]] && return 0
  done
  return 1
}

rmel() {
  local _a=$1 _b _c

  shift
  for _b; do
    [[ $_a != "$_b" ]] && _c="${_c:+$_c }$_b"
  done
  echo -n "$_c"
}

get_ifs() {
  local _if _if_list=$(rmel vlan $(ifconfig -C))

  for _if in $(ifconfig "$@" 2>/dev/null | sed '/^[a-z]/!d;s/:.*//'); do
    isin "${_if%%+([0-9])}" $_if_list || echo $_if
  done
}

# actually, set the hostname first.
xml_hn=$(xml sel -t -v 'metadata/domain/name' < "${IMD_MDDIR}/MDDATA.XML")
[[ "${xml_hn}" != '' ]] && { printf '%s\n' "${xml_hn}" > /etc/myname ; hostname "${xml_hn}" ; }

# prefix for xml metadata queries
pre="metadata/domain/devices/interface"
vl_pre='metadata/domain/metadata/untrustedhost:vlan'

# space-delim string for synthetic ifs to bring up.
synth_ifs=""

# default carp values base/skew
def_carp_ba='5' ; def_carp_sk='30'

# pf.conf includes
pf_dhclient="/etc/pf/dhclient.pf"
pf_ospf="/etc/pf/ospf.pf"
pf_carp="/etc/pf/carp.pf"
pf_intif_file="/etc/pf/int_if.macro"
pf_intif="${pf_intif_file}.new"
spnat_intif_file="/etc/pf/staticnat.macro"
spnat_intif="${spnat_intif_file}.new"
spnegnat_intif_file="/etc/pf/staticnat_neg.macro"
spnegnat_intif="${spnegnat_intif_file}.new"
pf_addrule_file="/etc/pf/additional.pf"
pf_addrule="${pf_addrule_file}.new"
printf '%s="{ ' int_if > "${pf_intif}"
printf '%s="{ ' staticnat_nets > "${spnat_intif}"
printf '%s="{ ' neg_staticnat_nets > "${spnegnat_intif}"
: > "${pf_addrule}"

# enumerate all the interfaces...
for iface in $(get_ifs) ; do
  xml_lla='' ; xml_label='' ; xml_mtu='' ; xml_ipv4=''
  calc_ip='' ; calc_subn='' ; calc_carp_ip='' ; calc_carp_subn=''
  carp_ip='' ; carp_vhid='' ; carp_pass='' ; carp_ba='' ; carp_sk=''
  xml_carp_ba='' ; xml_carp_sk=''

  # all matching is done via the MAC address.
  lla=$(ifconfig "${iface}" | awk '$1 == "lladdr" { print $2 }')
  ifq="${pre}[mac/@address=\"${lla}\"]"

  # check if in xml...
  xml_lla=$(xml sel -t -v "${ifq}/mac/@address" < "${IMD_MDDIR}/MDDATA.XML")

  # no? dhcp plz.
  [[ "${lla}" != "${xml_lla}" ]] && {
    printf 'pass quick on %s inet proto udp from (%s) port 67:68 to any port 67:68 keep state\n' "${iface}" "${iface}" >> "${pf_dhclient}"
    printf 'dhcp\n!rcctl start dhcpleased\n!sleep 1\n!dhcpleasectl -w 30 -l \\$if\n' > "/etc/hostname.${iface}" ; continue
  }
  : > "/etc/hostname.${iface}"

  # do I have a bridge name?
  # check the new vlan metadata path and use that if provided...
  xml_label=$(xml sel -N 'untrustedhost=https://untrusted.host/xml' -t -v "${vl_pre}"'/bridge[@mac="'"${lla}"'"]/@vlan' "${IMD_MDDIR}/MDDATA.XML")
  [[ "${xml_label}" == "" ]] && xml_label=$(xml sel -t -v "${ifq}/source/@bridge" < "${IMD_MDDIR}/MDDATA.XML")
  [[ "${xml_label}" != "" ]] && {
    echo "group ${xml_label}" >> "/etc/hostname.${iface}"
    case "${xml_label}" in
      egress|vmm) : ;;
      *) printf ' %s ' "${xml_label}" >> "${pf_intif}" ;;
    esac
  }

  # do I need to use static port nat here?
  xml_spnat=$(xml sel -t -v "${ifq}/nat/@type" < "${IMD_MDDIR}/MDDATA.XML")
  case "${xml_spnat}" in
    static)
      printf ' (%s:network) ' "${xml_label}" >> "${spnat_intif}"
      printf ' !(%s:network) ' "${xml_label}" >> "${spnegnat_intif}"
    ;;
  esac

  # should this interface be in an ospf area? wire pf.
  ospf_ct=$(xml sel -t -c "count(${ifq}/ospf/@area)" < "${IMD_MDDIR}/MDDATA.XML")
  [[ "${ospf_ct}" -gt 0 ]] && {
    printf 'pass in quick on %s proto ospf all\n' "${xml_label}" >> "${pf_ospf}"
    printf 'pass out quick on %s proto ospf all\n' "${xml_label}" >> "${pf_ospf}"
  }

  # do I have an mtu?
  xml_mtu=$(xml sel -t -v "${ifq}/mtu/@size" < "${IMD_MDDIR}/MDDATA.XML")
  [[ "${xml_mtu}" != "" ]] && echo "mtu ${xml_mtu}" >> "/etc/hostname.${iface}"

  # do I have an ipv4 address?
  xml_ipv4=$(xml sel -t -v "${ifq}/ipv4/@address" < "${IMD_MDDIR}/MDDATA.XML")
  [[ "${xml_ipv4}" == "" ]] && {
    printf 'pass quick on %s inet proto udp from (%s) port 67:68 to any port 67:68 keep state\n' "${iface}" "${iface}" >> "${pf_dhclient}"
    printf 'dhcp\n!rcctl start dhcpleased\n!sleep 1\n!dhcpleasectl -w 30 -l \\$if\n' >> "/etc/hostname.${iface}" ; continue
  }

  # from cidr to classic netmask *sigh*
  calc_subn=$(ipcalc "${xml_ipv4}" | awk -F': ' '$1 ~ "netmask" { split($2,s," ") ; print s[1] ; }')
  calc_ip=$(echo "${xml_ipv4}" | cut -d/ -f1)
  echo "inet ${calc_ip} ${calc_subn} NONE" >> "/etc/hostname.${iface}"

  # do I have a carp ip/vhid/pass?
  carp_ip=$(xml sel -t -v "${ifq}/carp4/@address" < "${IMD_MDDIR}/MDDATA.XML")
  [[ "${carp_ip}" != "" ]] && {
    printf 'pass quick on %s proto carp all\n' "${xml_label}" >> "${pf_carp}"
    carp_vhid=$(xml sel -t -v "${ifq}/carp4/@vhid" < "${IMD_MDDIR}/MDDATA.XML")
    carp_pass=$(xml sel -t -v "${ifq}/carp4/@pass" < "${IMD_MDDIR}/MDDATA.XML")
    [[ "${carp_vhid}" != "" ]] && [[ "${carp_pass}" != "" ]] && {
      calc_carp_subn=$(ipcalc "${carp_ip}" | awk -F': ' '$1 ~ "netmask" { split($2,s," ") ; print s[1] ; }')
      calc_carp_ip=$(echo "${carp_ip}" | cut -d/ -f1)
      xml_carp_ba=$(xml sel -t -v "${ifq}/carp4/@advbase" < "${IMD_MDDIR}/MDDATA.XML")
      xml_carp_sk=$(xml sel -t -v "${ifq}/carp4/@advskew" < "${IMD_MDDIR}/MDDATA.XML")
      carp_ba="${xml_carp_ba:-${def_carp_ba}}" ; carp_sk="${xml_carp_sk:-${def_carp_sk}}"
      : > "/etc/hostname.carp${carp_vhid}"
      [[ "${xml_mtu}" != "" ]] && echo "mtu ${xml_mtu}" >> "/etc/hostname.carp${carp_vhid}"
      echo "inet ${calc_carp_ip} ${calc_carp_subn} NONE vhid ${carp_vhid} advbase ${carp_ba} advskew ${carp_sk} carpdev ${iface} pass ${carp_pass}" >> "/etc/hostname.carp${carp_vhid}"
      [[ "${xml_label}" != "" ]] && echo "group ${xml_label}" >> "/etc/hostname.carp${carp_vhid}"
      synth_ifs="${synth_ifs} carp${carp_vhid}"
    }
  }
done

# close the pf macros
printf ' %s\n' '}"' >> "${pf_intif}"
mv "${pf_intif}" "${pf_intif_file}"
printf ' %s\n' '}"' >> "${spnat_intif}"
mv "${spnat_intif}" "${spnat_intif_file}"
printf ' %s\n' '}"' >> "${spnegnat_intif}"
mv "${spnegnat_intif}" "${spnegnat_intif_file}"

# do I need to flip the global NAT switch?
global_egr_nat=$(xml sel -t -v 'metadata/router/@egress-nat' < "${IMD_MDDIR}/MDDATA.XML")
[[ "${global_egr_nat}" == "true" ]] && {
  mv /etc/pf/egress-nat.dist /etc/pf/nat.pf
}

# handle additional firewall fules
add_rules_ct=$(xml sel -t -v 'count(/metadata/firewall/rule)' < "${IMD_MDDIR}/MDDATA.XML")

cur_rul=0
while [ "${cur_rul}" -lt "${add_rules_ct}" ] ; do
  _=$((cur_rul++)) # count up...
  # grab rule attributes
  rule_src='metadata/firewall/rule['"${cur_rul}"']'
  rule_op='' ; rule_if='' ; rule_proto='' ; rule_destports='' ; rule_srcports='' ; rule_source='' ; rule_dest='' ; rule_state='' ; rule_direction=''
  rule_op=$(xml sel -t -v "${rule_src}/@operation" < "${IMD_MDDIR}/MDDATA.XML")
  rule_direction=$(xml sel -t -v "${rule_src}/@direction" < "${IMD_MDDIR}/MDDATA.XML")
  [[ -n "${rule_direction}" ]] || rule_direction='in'
  rule_if=$(xml sel -t -v "${rule_src}/@interface" < "${IMD_MDDIR}/MDDATA.XML")
  [[ -n "${rule_if}" ]] && {
    # shellcheck disable=SC2001
    rule_if=$(echo "${rule_if}"|sed 's/,/ /')
    rule_if="on { ${rule_if} }"
  }
  rule_proto=$(xml sel -t -v "${rule_src}/@protocol" < "${IMD_MDDIR}/MDDATA.XML")
  case "${rule_proto}" in
    icmp) rule_proto="inet proto icmp" ;;
    tcp)  rule_proto="inet proto tcp"  ;;
    udp)  rule_proto="inet proto udp"  ;;
    *)    :                            ;;
  esac
  rule_source=$(xml sel -t -v "${rule_src}/@source" < "${IMD_MDDIR}/MDDATA.XML")
  [[ -n "${rule_source}" ]] || rule_source=any
  # shellcheck disable=SC2001
  rule_source=$(echo "${rule_source}"|sed 's/,/ /')
  rule_source="{ ${rule_source} }"
  rule_destports=$(xml sel -t -v "${rule_src}/@destports" < "${IMD_MDDIR}/MDDATA.XML")
  [[ -n "${rule_destports}" ]] && {
    # shellcheck disable=SC2001
    rule_destports=$(echo "${rule_destports}"|sed 's/,/ /')
    rule_destports="port { ${rule_destports} }"
  }
  rule_srcports=$(xml sel -t -v "${rule_src}/@sourceports" < "${IMD_MDDIR}/MDDATA.XML")
  [[ -n "${rule_srcports}" ]] && {
    # shellcheck disable=SC2001
    rule_srcports=$(echo "${rule_srcports}"|sed 's/,/ /')
    rule_srcports="port { ${rule_srcports} }"
  }
  rule_dest=$(xml sel -t -v "${rule_src}/@destination" < "${IMD_MDDIR}/MDDATA.XML" | xml unesc)
  [[ -n "${rule_dest}" ]] && {
    # shellcheck disable=SC2001
    rule_dest=$(echo "${rule_dest}"|sed 's/,/ /')
    rule_dest="{ ${rule_dest} }"
  }
  rule_state=$(xml sel -t -v "${rule_src}/@state" < "${IMD_MDDIR}/MDDATA.XML")
  [[ -n "${rule_state}" ]] && {
    rule_state="${rule_state} state"
  }
  {
    printf '%s %s %s %s from %s %s to %s %s %s\n' "${rule_op}" "${rule_direction}" "${rule_if}" "${rule_proto}" "${rule_source}" "${rule_srcports}" "${rule_dest}" "${rule_destports}" "${rule_state}"
  } >> "${pf_addrule}"
done

mv "${pf_addrule}" "${pf_addrule_file}"

# kick pf for new rules
pfctl -f /etc/pf.conf

# okay, now bring it all up!
for iface in $(get_ifs) ; do
  sh /etc/netstart "${iface}"
done

for iface in ${synth_ifs} ; do
  sh /etc/netstart "${iface}"
done

# kick pf again in case we needed to resolve labeled interfaces
pfctl -f /etc/pf.conf

exit 0
