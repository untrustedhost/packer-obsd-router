#!/usr/bin/env ksh

# lifted from install.subr
isin() {
  local _a=$1 _b

  shift
  for _b; do
    [[ $_a == "$_b" ]] && return 0
  done
  return 1
}

rmel() {
  local _a=$1 _b _c

  shift
  for _b; do
    [[ $_a != "$_b" ]] && _c="${_c:+$_c }$_b"
  done
  echo -n "$_c"
}

get_ifs() {
  local _if _if_list=$(rmel vlan $(ifconfig -C))

  for _if in $(ifconfig "$@" 2>/dev/null | sed '/^[a-z]/!d;s/:.*//'); do
    isin "${_if%%+([0-9])}" $_if_list || echo $_if
  done
}

# get a list of all interfaces and MAC addresses
if_mlist=''
for intf in $(get_ifs) ; do
  if_lla=''
  if_lla=$(ifconfig "${intf}" | awk '$1 == "lladdr" { print $2 ; }')
  if_mlist="${if_mlist} ${intf}:${if_lla}"
done

# find all the tagged dhcprelay interfaces to give dhcrelays to
if_pre='metadata/domain/devices/interface'

# set mask
umask 077

# grab all the relay destination addresses
dhcrelay_targets=''
dhcrelay_targets="$(xml sel -t -v metadata/dhcprelay/destination < "${IMD_MDDIR}/MDDATA.XML" | xargs)"

[[ "${dhcrelay_targets}" != "" ]] || exit 1

printf '%s\n' ${dhcrelay_targets} > /etc/pf/dhcpservers.table
pfctl -t dhcpservers -T replace -f /etc/pf/dhcpservers.table

# find all mac addresses with dhcprelay ipv4 service
dhcrelay_mlist=''
dhcrelay_mlist="$(xml sel -t -v 'metadata/domain/devices/interface[@type="bridge"][dhcprelay/@ipv4="true"]/mac/@address' < "${IMD_MDDIR}/MDDATA.XML")"

for maddr in ${dhcrelay_mlist} ; do
  ifn=''
  # find in if_mlist...
  for sysaddr in ${if_mlist} ; do
    case "${sysaddr}" in
      *":${maddr}") ifn=$(echo "${sysaddr}" | cut -d: -f1) ; continue ;;
    esac
  done
  [[ "${ifn}" != "" ]] && {
    # go find the bridge name to set as the remote name
    xp_brname=''
    xp_brname="$(xml sel -t -v 'metadata/domain/devices/interface[@type="bridge"][mac/@address="'"${maddr}"'"]/source/@bridge' < "${IMD_MDDIR}/MDDATA.XML")"
    [[ "${xp_brname}" != "" ]] && {
      ccid=$(echo "${ifn}" | tr -d [a-z])
      cp /etc/rc.d/dhcrelay "/etc/rc.d/dhcrelay_${ifn}"
      rcctl enable "dhcrelay_${ifn}"
      rcctl set "dhcrelay_${ifn}" flags "-C ${ccid} -R ${xp_brname} -i ${ifn} ${dhcrelay_targets}"
      rcctl start "dhcrelay_${ifn}"
    }
  }
done
