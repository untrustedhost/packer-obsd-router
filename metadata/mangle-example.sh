#!/usr/bin/env bash

source="$1"

# clunky double array construct for flexible labels
bridgename[0]='cyan'
bridgename[1]='magenta'

# this takes an ifindex and an octet-delin OSPF zone.
ospf_area[0]='0.0.0.0'

# OSPF keys can be in the range 1-255 - always set explicit IDs and have 2 available for resliency.
# maximum key length is 16. (apg -M SNCL -m 16) would probably still avoid things like...quotes.
# they will need to match all routers, this lets us roll keys by deploying a new slot/phrase though.
# these are broken out by area id via `printf '%02X' ${ospf_area//./ }`
# and will need to match an area associated in ospf_area
ospf_key_00000000[4]='MeitIf*ogaximut6'
ospf_key_00000000[7]='rids%Omwulv6Flew'

# ip addresses of dhcp servers for relay - currently globals as we model closer to ISC dhcp relay.

case "${source}" in
  *one*)
    ipv4[0]='192.168.2.33/24'
    ipv4[1]='192.168.0.33/24'
    ipv4[2]='192.168.7.33/24'
  ;;
  *two*)
    ipv4[0]='192.168.2.44/24'
    ipv4[1]='192.168.0.44/24'
    ipv4[2]='192.168.7.44/24'
  ;;
esac

# /31 for the dhcp server anycast-land

# bird should _really_ have a per-router id. use green's ip.
routerid="172.16.0.0"
asn="65535"

# shouldn't need to change these - paths for interface bridgenames
xp_bridge='/domain/devices/interface'
xp_bridgenames="${xp_bridge}/source/@bridge"

# array to hold all the edit args to xmlstarlet
xmlstarlet_args=()

# start by filtering out what _type_ of vm this is. ;)
node_filter=('domain/@type')

# filter out all manner of things based on el output now
while read -r nodeline ; do
  has_parent=0
  case "${nodeline}" in
    # keep the below nodes
    domain|domain/name|domain/devices|domain/devices/interface*) : ;;
    *) 
      # hm. is a parent in node_filter yet?
      potential="/${nodeline}"
      while [ "${potential}" != "" ] ; do
        # strip an element...
        potential="${potential%/*}"
        # explicit break if that _was_ the last element
        [[ -z "${potential}" ]] && break

        # compare to space-delim string of current entities
        case " ${node_filter[*]} " in
          *" ${potential#/} "*) has_parent=1 ;;
        esac
      done

      # if we didn't find a parent, add this now.
      [[ "${has_parent}" -eq 1 ]] || node_filter=("${node_filter[@]}" "${nodeline}")
    ;;
  esac
done < <(xmlstarlet el "${source}")

# turn _that_ into delete calls for xmlstarlet
for filt in "${node_filter[@]}" ; do
  xmlstarlet_args=("${xmlstarlet_args[@]}" "-d" "${filt}")
done

# walk the interfaces and add IPs, MTUs via xmlstarlet calls...
for bridge in $(xmlstarlet sel -t -v "${xp_bridgenames}" < "${source}") ; do
  # create ipv4 subnode and set address elem in it.
  ctr=0
  for label in "${bridgename[@]}" ; do
    [[ "${bridge}" == "${label}" ]] && {
      # non-zero ip...
      [[ "${ipv4[${ctr}]}" ]] && {
        xmlstarlet_args=("${xmlstarlet_args[@]}" 
          -s "${xp_bridge}[source/@bridge=\"${bridge}\"]" -t 'elem' -n 'ipv4' -v ''
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/ipv4" -t attr -n 'address' -v "${ipv4[${ctr}]}")
      }
      # mtu?
      [[ "${mtu[${ctr}]}" ]] && {
        xmlstarlet_args=("${xmlstarlet_args[@]}" 
          -s "${xp_bridge}[source/@bridge=\"${bridge}\"]" -t 'elem' -n 'mtu' -v ''
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/mtu" -t attr -n 'size' -v "${mtu[${ctr}]}")
      }
      # carp?
      [[ "${carp4[${ctr}]}" ]] && [[ "${carp4_id[${ctr}]}" ]] && [[ "${carp4_pw[${ctr}]}" ]] && {
        # assume default advbase of 20 if we got nothin' else
        # assume default advskew of 128(!) if we have nothing else
        xmlstarlet_args=("${xmlstarlet_args[@]}"
          -s "${xp_bridge}[source/@bridge=\"${bridge}\"]" -t 'elem' -n 'carp4' -v ''
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/carp4" -t attr -n 'address' -v "${carp4[${ctr}]}"
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/carp4" -t attr -n 'pass' -v "${carp4_pw[${ctr}]}"
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/carp4" -t attr -n 'vhid' -v "${carp4_id[${ctr}]}"
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/carp4" -t attr -n 'advbase' -v "${carp4_ba[${ctr}]:-20}"
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/carp4" -t attr -n 'advskew' -v "${carp4_sk[${ctr}]:-128}")
        case " ${distribute_ospf} " in
          *" ${bridge} "*) xmlstarlet_args=("${xmlstarlet_args[@]}"
                                          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/carp4" -t attr -n 'redistribute_ospf' -v 'true')
                         ;;
        esac
      }
      # dhcp relay?
      case " ${dhcp_relay_bridges} " in
        *" ${bridge} "*) xmlstarlet_args=("${xmlstarlet_args[@]}"
                                          -s "${xp_bridge}[source/@bridge=\"${bridge}\"]" -t elem -n 'dhcprelay' -v ''
                                          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/dhcprelay" -t attr -n 'ipv4' -v 'true')
      esac
      # ospf area?
      [[ "${ospf_area[${ctr}]}" ]] && {
        xmlstarlet_args=("${xmlstarlet_args[@]}"
          -s "${xp_bridge}[source/@bridge=\"${bridge}\"]" -t 'elem' -n 'ospf' -v ''
          -i "${xp_bridge}[source/@bridge=\"${bridge}\"]/ospf" -t attr -n 'area' -v "${ospf_area[${ctr}]}")
      }
    }
    ctr=$((ctr + 1))
  done
done

# move domain node inside metadata now
xmlstarlet_args=("${xmlstarlet_args[@]}"
                    -s / -t 'elem' -n "metadata" -v ''
                    -m //domain //metadata)

# dhcp relay configuration
[[ -n "${dhcp_relay_targets}" ]] && {
  xmlstarlet_args=("${xmlstarlet_args[@]}"
                   -s /metadata -t 'elem' -n 'dhcprelay' -v '')
  for i in ${dhcp_relay_targets} ; do
    xmlstarlet_args=("${xmlstarlet_args[@]}"
                     -s /metadata/dhcprelay -t 'elem' -n 'destination' -v "${i}")
  done
}

# router configuration
xmlstarlet_args=("${xmlstarlet_args[@]}"
    -s /metadata -t 'elem' -n 'router' -v ''
    -i /metadata/router -t attr -n 'id' -v "${routerid}"
    -i /metadata/router -t attr -n 'asn' -v "${asn}")

# ospf zone authentication configuration
ospf_seen=()
for area in "${ospf_area[@]}" ; do
  case " ${ospf_seen[*]} " in
    *" ${area} "*) continue ;;
  esac

  # convert area from octet to hex for lookup...
  area_hx=$(printf '%02X' ${area//./ })
  handle="ospf_key_${area_hx}"
  # this is...the only way to get the keys of an array dynamically AFAICT.
  id_keys=$(eval echo \${!${handle}[*]})

  # check the results, then go get passphrases
  [[ "${id_keys}" ]] && {
    xmlstarlet_args=("${xmlstarlet_args[@]}" -s /metadata/router -t 'elem' -n 'ospf' -v '' -i /metadata/router/ospf -t attr -n 'area' -v "${area}")
    for keyid in ${id_keys} ; do
      pass=$(eval echo \${${handle}[${keyid}]})
      xmlstarlet_args=("${xmlstarlet_args[@]}"
                         -s "/metadata/router/ospf[@area=\"${area}\"]" -t 'elem' -n 'authentication' -v ''
                         -s "/metadata/router/ospf[@area=\"${area}\"]/authentication[last()]" -t 'attr' -n 'key' -v "${keyid}"
                         -s "/metadata/router/ospf[@area=\"${area}\"]/authentication[@key=\"${keyid}\"]" -t 'attr' -n 'password' -v "${pass}")
    done
  }
  ospf_seen=("${ospf_seen[@]}" "${area}")
done

# finally, run xmlstarlet and enjoy.
xmlstarlet ed "${xmlstarlet_args[@]}" "${source}"
