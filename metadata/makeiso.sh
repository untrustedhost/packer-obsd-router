#!/usr/bin/env bash

output=config.iso

here="$(pwd)"
scratch="$(mktemp -d)"

./mangle-example.sh "$1" > "${scratch}/MDDATA.XML"

(cd "${scratch}" && genisoimage -o "${here}/${2}" -rational-rock -V IMD .)

rm -rf "${scratch}"

