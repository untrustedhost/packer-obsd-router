OBSD_VERSION := 7.1
OBSD_ARCH := amd64
OBSD_NV := $(subst .,,$(OBSD_VERSION))
OB_PATH := obsd_dist/$(OBSD_VERSION)/$(OBSD_ARCH)

# NOTE: this target will never validate. that's ok, the script will cache.
$(OB_PATH)/packages/%: support/verify-or-dl-pkgset.sh
	env OBSD_VERSION=$(OBSD_VERSION) ./support/verify-or-dl-pkgset.sh $(notdir $@)

$(OB_PATH)/%: support/verify-or-dl.sh
	env OBSD_VERSION=$(OBSD_VERSION) ./support/verify-or-dl.sh $(notdir $@)

obsd_dist/syspatch/$(OBSD_VERSION)/$(OBSD_ARCH)/syspatch: support/verify-or-dl.sh
	-env OBSD_VERSION=$(OBSD_VERSION) ./support/verify-or-dl.sh $(notdir $@)

obsd_dist/$(OBSD_VERSION)/%: support/verify-or-dl.sh
	env OBSD_VERSION=$(OBSD_VERSION) ./support/verify-or-dl.sh $(notdir $@)

obsd_dist/patches/$(OBSD_VERSION).tar.gz: support/verify-or-dl.sh
	env OBSD_VERSION=$(OBSD_VERSION) ./support/verify-or-dl.sh srcpatch

standard-deps: $(OB_PATH)/base$(OBSD_NV).tgz $(OB_PATH)/bsd $(OB_PATH)/bsd.rd $(OB_PATH)/pxeboot obsd_dist/syspatch/$(OBSD_VERSION)/$(OBSD_ARCH)/syspatch $(OB_PATH)/packages/xmlstarlet $(OB_PATH)/packages/ipcalc $(OB_PATH)/packages/augeas $(OB_PATH)/packages/bash $(OB_PATH)/packages/qemu-ga

%-deps:
	true

er-deps: $(OB_PATH)/packages/miniupnpd

scripts/%: standard-deps %-deps
	cd scripts && $(MAKE) $(notdir $@)

packer/%.qcow2.xz: scripts/%.tar
	-rm -rf packer/$*.qcow2.xz
	-rm -rf packer/output-qemu-$*
	$(MAKE) standard-deps
	$(MAKE) $*-deps
	cd packer && $(MAKE) OBSD_VERSION=$(OBSD_VERSION) CONFIG=$* PROVISIONER_TAR=../scripts/$*.tar $*.qcow2.xz

metadata/%.xml:
	cd metadata && $(MAKE) $*.xml

metadata/%.iso:
	cd metadata && $(MAKE) $*.iso

# this...mess helps make BASE required for %-interactive
ifeq ($(BASE),)
%-interactive:
	$(error BASE is not set)
else
.SECONDARY: packer/$(BASE).qcow2.xz

%-interactive: packer/$(BASE).qcow2.xz metadata/%.xml metadata/%.iso ./support/test-instance.sh
	xzcat $< > $*.qcow2
	./support/test-instance.sh metadata/$*.xml $*.qcow2 metadata/$*.iso
	-rm $*.qcow2
endif
